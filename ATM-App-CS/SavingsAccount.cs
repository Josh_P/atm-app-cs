﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATM_App_CS
{
    class SavingsAccount : account
    {
        /// <summary>
        /// Sets new annual interest rate
        /// </summary>
        /// <param name="newIntrRate"></param>
        public new void setAnnualIntrRate(float newIntrRate)
        {
            _annualIntrRate = newIntrRate;
        }
        /// <summary>
        /// Adds money to balance
        /// </summary>
        /// <param name="amount"></param>
        /// <returns>Balance plus amount deposited</returns>
        public new float deposit(float amount)
        {
            return _balance + amount;
        }
    }
}
