﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATM_App_CS
{
    class account
    {
        /// <summary>
        /// Define variables
        /// </summary>
        private string _acctHolderName;

        private int _acctNo;

        private float _annualIntrRate;

        private float _balance;
        /// <summary>
        /// Initilizes variables
        /// </summary>
        /// <param name="acctNo"></param>
        /// <param name="acctHolderName"></param>
        public void __init__(int acctNo, string acctHolderName)
        {
        }
        /// <summary>
        /// Get the account holder name and write to _acctHolderName
        /// </summary>
        /// <returns></returns>
        public string getAccountHolderName()
        {
            return _acctHolderName;
        }
        /// <summary>
        /// Get account holder number and write to _acctNo
        /// </summary>
        /// <returns></returns>
        public int getAccountHolderNo()
        {
            return _acctNo;
        }
        /// <summary>
        /// Get the annual interest rate and write to _annualIntrRate
        /// </summary>
        /// <returns></returns>
        public float getAnnualIntrRate()
        {
            return _annualIntrRate;
        }
        /// <summary>
        /// Set the annual interest rate
        /// </summary>
        /// <param name="newIntrRate"></param>
        public void setAnnualIntrRate(float newIntrRate)
        {
            _annualIntrRate = newIntrRate;
        }
        /// <summary>
        /// Call the account balance
        /// </summary>
        /// <returns>Amount of money in account</returns>
        public float getBalance()
        {
            return _balance;
        }
        /// <summary>
        /// Add money to the account
        /// </summary>
        /// <param name="amount"></param>
        /// <returns>Balance plus deposited amount</returns>
        public float deposit(float amount)
        {
            return _balance + amount;
        }
        /// <summary>
        /// Remove money from the account
        /// </summary>
        /// <param name="amount"></param>
        /// <returns>Balance minus the amount withdrawn</returns>
        public float withdraw(float amount)
        {
            return _balance - amount;
        }
        /*
        public void load(TextIOWrapper)
        {
        }

        public void save(TextIOWrapper)
        {
        }
        */
    }
}
