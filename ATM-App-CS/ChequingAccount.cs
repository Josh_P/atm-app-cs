﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATM_App_CS
{
    class ChequingAccount : account
    {
        /// <summary>
        /// Sets new interest rate
        /// </summary>
        /// <param name="newIntrRate"></param>
        public new void setAnnualIntrRate(float newIntrRate)
        {
            account._annualIntrRate = newIntrRate; //??? fix later???
        }
        /// <summary>
        /// Removes money from balance
        /// </summary>
        /// <param name="amount"></param>
        /// <returns>Balance minus amount withdrawn</returns>
        public new float withdraw(float amount)
        {
            return account._balance - amount; //??? fix later???
        }
    }
}
